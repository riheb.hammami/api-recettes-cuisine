<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Repository\IngredientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class IngredientController extends AbstractController
{
    private $ingredientRepository;
    private $em;
    private $serializer;
    public function __construct(IngredientRepository $ingredientRepository, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->ingredientRepository = $ingredientRepository;
        $this->serializer = $serializer;
        $this->em = $em;
    }

    /**
     * @Route("/api/ingredients", name="create_ingredient",methods={"POST"})
     */
    public function createIngredient(Request $request, ValidatorInterface $validator)
    {
        $data = $request->getContent();
        try {
            $ingredient = $this->serializer->deserialize($data, Ingredient::class, 'json');

            $errors = $validator->validate($ingredient);

            if (count($errors) > 0) {
                return $this->json($errors, 400);
            }

            $this->em->persist($ingredient);
            $this->em->flush();

            return $this->json($ingredient, 201, [], ['groups' => 'ingredient']);
        } catch (NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }
    /**
     * @Route("/api/ingredients", name="get_ingredients",methods={"GET"})
     */
    public function getIngredients()
    {
        return $this->json($this->ingredientRepository->findAll(), 200, [], ['groups' => 'ingredient']);
    }

    /**
     * @Route("/api/ingredients/{id}", name="get_ingredient", methods={"GET"})
     */
    public function get($id): JsonResponse
    {
        return $this->json($this->ingredientRepository->findOneBy(['id' => $id]), 200, [], ['groups' => 'ingredient']);
    }


    /**
     * @Route("/api/ingredients/{id}", name="update_ingredient",methods={"PUT"})
     */
    public function updateIngredient($id, Request $request, ValidatorInterface $validator)
    {
        $ingredient = $this->ingredientRepository->findOneBy(['id' => $id]);
        $data = $request->getContent();
        try {
            $newIngredient = $this->serializer->deserialize($data, Ingredient::class, 'json');

            $errors = $validator->validate($newIngredient);

            if (count($errors) > 0) {
                return $this->json($errors, 400);
            }
            $ingredient->setNom($newIngredient->getNom());
            $this->em->flush();

            return $this->json($ingredient, 200, [], ['groups' => 'ingredient']);
        } catch (NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * @Route("/api/ingredients/{id}", name="delete_ingredient",methods={"DELETE"})
     */
    public function deleteIngredient($id)
    {
        $ingredient = $this->ingredientRepository->findOneBy(['id' => $id]);
        $this->em->remove($ingredient);
        $this->em->flush();
        return $this->json("", 204);
    }
}
