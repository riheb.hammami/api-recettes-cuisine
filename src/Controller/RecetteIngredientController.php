<?php

namespace App\Controller;

use App\Entity\RecetteIngredient;
use App\Repository\IngredientRepository;
use App\Repository\RecetteIngredientRepository;
use App\Repository\RecetteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RecetteIngredientController extends AbstractController
{
    private $recetteIngredientRepository;
    private $recetteRepository;
    private $ingredientRepository;
    private $serializer;
    private $em;

    public function __construct(RecetteIngredientRepository $recetteIngredientRepository, IngredientRepository $ingredientRepository, RecetteRepository $recetteRepository, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->recetteIngredientRepository = $recetteIngredientRepository;
        $this->recetteRepository = $recetteRepository;
        $this->ingredientRepository = $ingredientRepository;
        $this->em = $em;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/recette_ingredient", name="create_recette_ingredient",methods={"POST"})
     */
    public function createRecetteIngredient(Request $request, EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $obj = json_decode($request->getContent());
        $recette = $this->recetteRepository->findOneBy(['id' => $obj->{'recette'}]);
        $ingredient = $this->ingredientRepository->findOneBy(['id' => $obj->{'ingredient'}]);


        try {

            $data = $this->serializer->deserialize($request->getContent(), RecetteIngredient::class, 'json');

            $errors = $validator->validate($data);

            if (count($errors) > 0) {
                return $this->json($errors, 400);
            }


            $recetteIngredient = new RecetteIngredient();
            $recetteIngredient->setIngredient($ingredient);
            $recetteIngredient->setRecette($recette);
            $recetteIngredient->setQuantite($data->getQuantite());
            $recetteIngredient->setOrdre($data->getOrdre());
            $em->persist($recetteIngredient);
            $em->flush();

            return $this->json($recetteIngredient, 201, [], ['groups' => 'recette_ingredient']);
        } catch (NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * @Route("/api/recette_ingredient", name="get_recette_ingredients",methods={"GET"})
     */
    public function getIngredients()
    {
        return $this->json($this->recetteIngredientRepository->findAll(), 200, [], ['groups' => 'recette_ingredient']);
    }

    /**
     * @Route("/api/recette_ingredient/{id}", name="get_recette_ingredient", methods={"GET"})
     */
    public function get($id): JsonResponse
    {
        return $this->json($this->recetteIngredientRepository->findOneBy(['id' => $id]), 200, [], ['groups' => 'recette_ingredient']);
    }

    /**
     * @Route("/api/recette_ingredient/{id}", name="update_recette_ingredient",methods={"PUT"})
     */
    public function updateRecetteIngredient($id, Request $request, ValidatorInterface $validator)
    {
        $recetteIngredient = $this->recetteIngredientRepository->findOneBy(['id' => $id]);

        $obj = json_decode($request->getContent());
        $recette = $this->recetteRepository->findOneBy(['id' => $obj->{'recette'}]);
        $ingredient = $this->ingredientRepository->findOneBy(['id' => $obj->{'ingredient'}]);


        try {

            $data = $this->serializer->deserialize($request->getContent(), RecetteIngredient::class, 'json');

            $errors = $validator->validate($data);

            if (count($errors) > 0) {
                return $this->json($errors, 400);
            }


            $recetteIngredient->setIngredient($ingredient);
            $recetteIngredient->setRecette($recette);
            $recetteIngredient->setQuantite($data->getQuantite());
            $recetteIngredient->setOrdre($data->getOrdre());
            $this->em->flush();

            return $this->json($recetteIngredient, 200, [], ['groups' => 'recette_ingredient']);
        } catch (NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * @Route("/api/recette_ingredient/{id}", name="delete_recette_ingredient",methods={"DELETE"})
     */
    public function deleteRecetteIngredient($id)
    {
        $recetteIngredient = $this->recetteIngredientRepository->findOneBy(['id' => $id]);
        $this->em->remove($recetteIngredient);
        $this->em->flush();
        return $this->json("", 204);
    }
}
