<?php

namespace App\Controller;

use App\Entity\Recette;
use App\Repository\RecetteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class RecetteController extends AbstractController
{

    private $recetteRepository;
    private $em;
    private $serializer;

    public function __construct(RecetteRepository $recetteRepository, EntityManagerInterface $em, SerializerInterface $serializer)
    {
        $this->recetteRepository = $recetteRepository;
        $this->serializer = $serializer;
        $this->em = $em;
    }

    /**
     * @Route("/api/recettes", name="create_recette",methods={"POST"})
     */
    public function createRecette(Request $request, ValidatorInterface $validator)
    {
        $data = $request->getContent();

        try {
            $recette = $this->serializer->deserialize($data, Recette::class, 'json');

            $errors = $validator->validate($recette);

            if (count($errors) > 0) {
                return $this->json($errors, 400);
            }

            $this->em->persist($recette);
            $this->em->flush();

            return $this->json($recette, 201, [], ['groups' => 'recette']);
        } catch (NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }
    /**
     * @Route("/api/recettes", name="get_recettes",methods={"GET"})
     */
    public function getRecettes()
    {
        return $this->json($this->recetteRepository->findAll(), 200, [], ['groups' => 'recette']);
    }

    /**
     * @Route("/api/recettes/{id}", name="get_recette", methods={"GET"})
     */
    public function get($id): JsonResponse
    {
        return $this->json($this->recetteRepository->findOneBy(['id' => $id]), 200, [], ['groups' => 'recette']);
    }


    /**
     * @Route("/api/recettes/{id}", name="update_recette",methods={"PUT"})
     */
    public function updateRecette($id, Request $request, ValidatorInterface $validator)
    {
        $recette = $this->recetteRepository->findOneBy(['id' => $id]);
        $data = $request->getContent();
        try {
            $newRecette = $this->serializer->deserialize($data, Recette::class, 'json');

            $errors = $validator->validate($newRecette);

            if (count($errors) > 0) {
                return $this->json($errors, 400);
            }
            $recette->setTitre($newRecette->getTitre());
            $recette->setSousTitre($newRecette->getSousTitre());
            $this->em->flush();

            return $this->json($recette, 200, [], ['groups' => 'recette']);
        } catch (NotEncodableValueException $e) {
            return $this->json([
                'status' => 400,
                'message' => $e->getMessage()
            ], 400);
        }
    }

    /**
     * @Route("/api/recettes/{id}", name="delete_recette",methods={"DELETE"})
     */
    public function deleteRecette($id)
    {
        $recette = $this->recetteRepository->findOneBy(['id' => $id]);
        $this->em->remove($recette);
        $this->em->flush();
        return $this->json("", 204);
    }
}
