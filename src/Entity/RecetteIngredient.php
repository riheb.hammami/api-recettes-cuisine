<?php

namespace App\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass=App\Repository\RecetteIngredientRepository::class)
 */
class RecetteIngredient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"recette","recette_ingredient"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Groups({"recette","recette_ingredient"})
     */
    private $quantite;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     * @Groups({"recette","recette_ingredient"})
     */
    private $ordre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Recette", cascade={"persist"})
     * @Groups("recette_ingredient")
     */
    private $recette;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ingredient", cascade={"persist"})
     * @Groups({"recette","recette_ingredient"})
     */
     private $ingredient;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?string
    {
        return $this->quantite;
    }

    public function setQuantite(string $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(int $ordre): self
    {
        $this->ordre = $ordre;

        return $this;
    }

    public function getRecette(): ?Recette
    {
        return $this->recette;
    }

    public function setRecette(?Recette $recette): self
    {
        $this->recette = $recette;

        return $this;
    }

    public function getIngredient(): ?Ingredient
    {
        return $this->ingredient;
    }

    public function setIngredient(?Ingredient $ingredient): self
    {
        $this->ingredient = $ingredient;

        return $this;
    }
}
